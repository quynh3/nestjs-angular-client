import { Component, OnInit } from '@angular/core';
import { HeroService } from '../hero.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-hero',
  templateUrl: './edit-hero.component.html',
  styleUrls: ['./edit-hero.component.scss']
})
export class EditHeroComponent implements OnInit {
  hero = {
    name: '',
    universe: ''
  };

  id: string;

  constructor(
    private heroService: HeroService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.id);
    if (this.id) {
      this.heroService.getHero(this.id).subscribe(data => {
        // console.log(data);
        this.hero = data;
      });
    }
  }

  onSave(form) {
    console.log(form);
    const data = form.value;
    if (this.id) {
      this.heroService.updateHero(this.id, data).subscribe(hero => {
        this.snackBar.open('Hero Updated');
        console.log(hero);
        this.router.navigateByUrl('/hero');
      });
    } else {
      this.heroService.createHero(data).subscribe(hero => {
        this.snackBar.open('Hero Created');
        console.log(hero);
        this.router.navigateByUrl('/hero');
      });
    }
  }
}
