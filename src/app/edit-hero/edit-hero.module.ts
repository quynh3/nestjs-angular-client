import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditHeroRoutingModule } from './edit-hero-routing.module';
import { EditHeroComponent } from './edit-hero.component';
import { MaterialModule } from '../material.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditHeroComponent],
  imports: [CommonModule, EditHeroRoutingModule, MaterialModule, FormsModule]
})
export class EditHeroModule {}
