import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hero } from './hero';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const baseUrl = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  constructor(private http: HttpClient) {}

  getData(): Observable<Hero[]> {
    return this.http
      .get<Hero[]>(`${baseUrl}/hero`)
      .pipe(tap(data => console.log('Hero: ', data)));
  }

  getHero(id: string): Observable<Hero> {
    return this.http
      .get<Hero>(`${baseUrl}/hero/${id}`)
      .pipe(tap(data => console.log('Hero: ', data)));
  }

  createHero(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(`${baseUrl}/hero`, hero);
  }

  updateHero(id: string, hero: Hero): Observable<Hero> {
    return this.http.put<Hero>(`${baseUrl}/hero/${id}`, hero);
  }

  deleteHero(id: string) {
    return this.http.delete(`${baseUrl}/hero/${id}`);
  }
}
