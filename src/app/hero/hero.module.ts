import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeroRoutingModule } from './hero-routing.module';
import { HeroComponent } from './hero.component';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [HeroComponent],
  imports: [CommonModule, HeroRoutingModule, MaterialModule]
})
export class HeroModule {}
