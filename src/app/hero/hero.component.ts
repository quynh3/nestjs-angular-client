import { Component, OnInit } from '@angular/core';
import { HeroService } from '../hero.service';
import { Observable } from 'rxjs';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {
  listHero: Observable<Hero[]>;
  constructor(private heroService: HeroService) {}

  ngOnInit(): void {
    // this.subscription = this.heroService.getData().subscribe(data => {
    //   console.log(data);
    //   this.hero = data;
    // });
    this.getData();
  }

  getData() {
    this.listHero = this.heroService.getData();
  }

  getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return { background: color };
  }

  onDelete(id) {
    console.log(id);
    this.heroService.deleteHero(id).subscribe(data => {
      this.getData();
      console.log('Hero Deleted');
    });
  }
}
