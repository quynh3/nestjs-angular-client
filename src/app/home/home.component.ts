import { Component, OnInit } from '@angular/core';
import { HeroService } from '../hero.service';
import { Hero } from '../hero';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  hero: Observable<Hero>;
  constructor(private heroService: HeroService) {}

  ngOnInit(): void {
    // this.hero = this.heroService.getHero('5d5cd4ddb141e240f02be8d4');
  }
}
