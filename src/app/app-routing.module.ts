import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'hero',
    loadChildren: () =>
      import('./hero/hero.module').then(m => m.HeroModule)
  },
  {
    path: 'edit',
    loadChildren: () =>
      import('./edit-hero/edit-hero.module').then(m => m.EditHeroModule)
  },
  {
    path: 'edit/:id',
    loadChildren: () =>
      import('./edit-hero/edit-hero.module').then(m => m.EditHeroModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
